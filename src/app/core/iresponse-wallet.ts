export interface IResponseWallet {
  error: string;
  address: string;
  bnb_balance: string;
  btc_balance: number;
  ether_balance: string;
  satt_balance: string;
  version: number;
  err: string;
}
